#ifndef DICTIONNARY_H
#define DICTIONNARY_H

#include <string_view>
#include <array>

const auto dictionnary = std::array{"TONUS", "LIEGE", "SABLE", "SAUNA", "RHUME", "DIEUX", "TACHE", "BANJO", "SUCRE", "CAPOT", "FRISE", "EMAIL", "LASER", "ECHEC",
                            "GLACON", "FRIMER", "COULIS", "JAUGER", "CRAMPE", "RANCIR", "DEFIER", "MORUES", "CIMENT", "JOUJOU", "DESSIN", "PIMENT",
                            "GESTION", "COULEUR", "DOMPTER", "PAPYRUS", "HAUTAIN", "RONFLER", "TACTILE", "SPOLIER", "DOYENNE", "COUVENT", "GLACIER",
                            "EPLUCHER", "ORPHELIN", "ENSABLER", "LEGITIME", "CHARMEUR", "RAYONNER", "BATONNET", "GRILLAGE", "ENDOSSER", "ANCIENNE"};

const auto LENGTH_DICTIONNARY = static_cast<int>(dictionnary.size());

#endif // DICTIONNARY_H
