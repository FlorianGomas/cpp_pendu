// Le Pendu.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
/*
	Title : Le pendu
	@author : Florian GOMAS
	Last Update : 02/05/20
*/


#include <iostream>
#include <Windows.h>
#include "Dictionnary.h"
#include "Menu.h"
#include "Game.h"

using namespace std;
bool continu = false;

int main()
{
	SetConsoleOutputCP(1252); //Permet d'afficher les accents dans la console
    displayMenu(continu);
	while (continu)
	{
		playGame();
		restart(continu);
	}

	if (!continu) {
		return EXIT_SUCCESS;
	}
}

