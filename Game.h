#pragma once

#include <string>

using namespace std;

void displayRules();
void playGame();
void testLetter(char& letter, string &word, string &hiddenWord);
void restart(bool &continu);
