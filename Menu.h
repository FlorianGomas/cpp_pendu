#pragma once

void displayMenu(bool &continu);

enum class Menuchoice
{
	RULES = '1',
	PLAY = '2',
	QUIT = '3',
	INCORRECT
};

Menuchoice askChoice();
