#include <iostream>
#include <string>
#include "Menu.h"
#include "Game.h"

using namespace std;

void displayMenu(bool &continu) {

	// Affichage du menu dans la console
	cout << "**********************************" << endl;
	cout << "*                                *" << endl;
	cout << "* Bienvenue dans le jeu du Pendu *" << endl;
	cout << "*                                *" << endl;
	cout << "**********************************" << endl <<endl;

	cout << "***************MENU***************" << endl;
	cout << "*                                *" << endl;
	cout << "* 1.R�gle du jeu                 *" << endl;
	cout << "*                                *" << endl;
	cout << "* 2.Lancer une partie            *" << endl;
	cout << "*                                *" << endl;
	cout << "* 3.Quitter                      *" << endl;
	cout << "*                                *" << endl;
	cout << "**********************************" << endl << endl;


	// Demande � l'utilisateur de faire un choix dans le menu + contr�le erreur de saisie
	Menuchoice choice;
	do {
		choice = askChoice();

		switch (choice)
		{
		case Menuchoice::RULES:
			displayRules();
			continu = true;
			break;
		case Menuchoice::PLAY:
			continu = true;
			break;
		case Menuchoice::QUIT:
			continu = false;
			cout << endl << "A bient�t !" << endl;
			break;
		case Menuchoice::INCORRECT:
			cout << endl << "D�sol� mais votre choix n'est pas correct." << endl;
			cout << "Merci de presser la touche 1,2 ou 3 puis de faire entr�e." << endl;
		default:
			break;
		}
	} while (choice == Menuchoice::INCORRECT);
}

Menuchoice askChoice() {
	char choice;
	cout << "Choisissez votre action : ";
	cin >> choice;

	if (choice == static_cast<char>(Menuchoice::RULES) || choice == static_cast<char>(Menuchoice::PLAY) || choice == static_cast<char>(Menuchoice::QUIT))
	{
		return static_cast<Menuchoice>(choice);
	}
	else
	{
		return Menuchoice::INCORRECT;
	}
}
