// Dessin du pendu

#include <iostream>
#include "Game.h"

using namespace std;

void draw(int& counter) {
	switch (counter) {
	case 10:
		cout << endl << endl;
		cout << "__ __" << endl << endl;;
		break;
	case 9:
		cout << endl << endl;
		cout << "  |  " << endl;
		cout << "  |  " << endl;
		cout << "  |  " << endl;
		cout << "  |  " << endl;
		cout << "  |  " << endl;
		cout << "__|__" << endl << endl;
		break;
	case 8:
		cout << endl << endl;
		cout << "  ______  " << endl;
		cout << "  |       " << endl;
		cout << "  |       " << endl;
		cout << "  |       " << endl;
		cout << "  |       " << endl;
		cout << "  |       " << endl;
		cout << "__|__     " << endl << endl;
		break;
	case 7:
		cout << endl << endl;
		cout << "  ______  " << endl;
		cout << "  |/      " << endl;
		cout << "  |       " << endl;
		cout << "  |       " << endl;
		cout << "  |       " << endl;
		cout << "  |       " << endl;
		cout << "__|__     " << endl << endl;
		break;
	case 6:
		cout << endl << endl;
		cout << "  ______  " << endl;
		cout << "  |/   |  " << endl;
		cout << "  |       " << endl;
		cout << "  |       " << endl;
		cout << "  |       " << endl;
		cout << "  |       " << endl;
		cout << "__|__     " << endl << endl;
		break;
	case 5:
		cout << endl << endl;
		cout << "  ______  " << endl;
		cout << "  |/   |  " << endl;
		cout << "  |    0  " << endl;
		cout << "  |       " << endl;
		cout << "  |       " << endl;
		cout << "  |       " << endl;
		cout << "__|__     " << endl << endl;
		break;
	case 4:
		cout << endl << endl;
		cout << "  ______  " << endl;
		cout << "  |/   |  " << endl;
		cout << "  |    0  " << endl;
		cout << "  |    |  " << endl;
		cout << "  |       " << endl;
		cout << "  |       " << endl;
		cout << "__|__     " << endl << endl;
		break;
	case 3:
		cout << endl << endl;
		cout << "  ______  " << endl;
		cout << "  |/   |  " << endl;
		cout << "  |    0  " << endl;
		cout << "  |   /|  " << endl;
		cout << "  |       " << endl;
		cout << "  |       " << endl;
		cout << "__|__     " << endl << endl;
		break;
	case 2:
		cout << endl << endl;
		cout << "  ______  " << endl;
		cout << "  |/   |  " << endl;
		cout << "  |    0  " << endl;
		cout << "  |   /|\\ " << endl;
		cout << "  |       " << endl;
		cout << "  |       " << endl;
		cout << "__|__     " << endl << endl;
		break;
	case 1:
		cout << endl << endl;
		cout << "  ______  " << endl;
		cout << "  |/   |  " << endl;
		cout << "  |    0  " << endl;
		cout << "  |   /|\\ " << endl;
		cout << "  |   /   " << endl;
		cout << "  |       " << endl;
		cout << "__|__     " << endl << endl;
		break;
	case 0:
		cout << endl << endl;
		cout << "  ______  " << endl;
		cout << "  |/   |  " << endl;
		cout << "  |    0  " << endl;
		cout << "  |   /|\\ " << endl;
		cout << "  |   / \\ " << endl;
		cout << "  |       " << endl;
		cout << "__|__     " << endl << endl;
		break;
	default:
		break;
	}
}