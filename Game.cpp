#include <iostream>
#include <string>
#include <string_view>
#include <random>
#include "Dictionnary.h"
#include "Game.h"
#include "Menu.h"
#include "Drawing.h"

using namespace std;

// Ici on gen�re un nombre al�atoire compris entre 0 et la taille du dictionnaire, qui permettra de choisir un mot de mani�re al�atoire
auto randomGen = random_device{};
auto generator = mt19937{ randomGen() };
auto distribution = uniform_int_distribution{ 0, LENGTH_DICTIONNARY };

bool letterFind = false;


void displayRules() {
	cout << endl;
	cout << "**************** Voici les r�gles du jeu du pendu ****************" << endl;
	cout << "* Dans ce jeu vous disposez de 11 coups pour trouver le mot cach�*" << endl;
	cout << "* Le nombre de caract�re \"*\" repr�sente le nombre de lettres �   *" << endl;
	cout << "* trouver. Pour chaque coup r�ussi, la lettre choisie s'affiche  *" << endl;
	cout << "* autant de fois qu'elle est pr�sente dans le mot. Pour chaque   *" << endl;
	cout << "* coup rat�, un pendu se dessine � l'�cran, si le dessin ce      *" << endl;
	cout << "* termine, vous avez perdu. Bonne chance !                       *" << endl;
	cout << "******************************************************************" << endl << endl;
}


void playGame() {
	string word = dictionnary[distribution(generator)];
	string hiddenWord = string(word.length(), '*');
	char letter;
	int counter = 10;

	// Tant que le mot n'est pas trouv� et que le compteur de coup n'est pas � z�ro, on joue
	while (hiddenWord != word && counter >= 0) {
		cout << endl << "Voici le mot � trouver : " << hiddenWord << endl;
		cout << "Saisissez une lettre : ";
		cin >> letter;
		letter = toupper(letter);
		testLetter(letter, word, hiddenWord);

		if (letterFind == false) {
			draw(counter);
			counter--;
		}
	}

	if (counter < 0 ) {
		cout << endl << "!!! Vous avez perdu !!!" << endl;
		cout << "Le mot � trouver �tait : " << word << endl << endl;
	}

	if (hiddenWord == word)
	{
		cout << endl << "!! VOUS AVEZ GAGNE !!" << endl << endl;
		cout << "Le mot cach� �tait : " << word << endl<< endl;
	}

}

// Fonction qui permet de tester si la lettre saisie est dans le mot � trouver. Si c'est le cas, elle affiche la lettre dans le mot cach�
void testLetter(char& letter, string& word, string& hiddenWord) {

	letterFind = false;
	for (int i = 0; i < word.length(); i++) {
		if (letter == word[i]) {
			hiddenWord[i] = word[i];
			letterFind = true;
		}
	}
}


void restart(bool& continu) {
	cout << "Voulez-vous refaire une partie ?" << endl;
	cout << "1. Oui" << endl;
	cout << "2. Non" << endl;
	int choice;

	cin >> choice;

	switch (choice){
	case 1:
		continu = true;
		break;
	case 2:
		continu = false;
		cout << endl << "A bient�t !" << endl;
		break;
	default:
		cout << endl << "Merci de saisir une valeur correct !" << endl;
		break;
	}
}
